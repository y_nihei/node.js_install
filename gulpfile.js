var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(){
    //scssディレクトリの指定
    gulp.src('./src/*.scss')
    //コンパイル実行
    .pipe(sass({style : 'expanded'})) //出力形式の種類　#nested, compact, compressed, expanded.
    //出力先の指定
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', () => {
	gulp.watch('./src/*.scss', ['styles']);
})

gulp.task('default', ['watch']);